package com.rcl.core.usecase;

import com.rcl.core.entity.Currency;
import com.rcl.core.entity.Order;
import com.rcl.core.entity.Transaction;
import com.rcl.core.iobound.RCLRequest;
import com.rcl.core.repository.CurrencyRepositoryInterface;
import com.rcl.core.repository.OrderRepositoryInterface;
import com.rcl.core.repository.TransactionRepositoryInterface;

public class AddTransaction {
	
	private TransactionRepositoryInterface transRepo;
	private CurrencyRepositoryInterface currRepo;
	private OrderRepositoryInterface orderRepo;
	
	public AddTransaction(TransactionRepositoryInterface transRepo, 
			CurrencyRepositoryInterface currRepo,
			OrderRepositoryInterface orderRepo) {
		this.transRepo = transRepo;
		this.currRepo = currRepo;
		this.orderRepo = orderRepo;
	}
	
	public Integer processOrderPaymentTransaction(RCLRequest req) throws Exception {
		// order id
		int orderId = Integer.parseInt(req.getData().get("order_id").toString());
		// currency id
		int currencyId = Integer.parseInt(req.getData().get("currency_id").toString());
		// payment_mode
		String paymentMode = req.getData().get("payment_mode").toString();
		// amount
		Double payAmount = Double.parseDouble(req.getData().get("pay_amount").toString());
		
		Transaction transaction = new Transaction();
		Order order = orderRepo.getOrder(orderId);
		Currency curr = currRepo.getCurrenct(currencyId);
		transaction.setOrder(order);
		transaction.setOrderCurrency(curr);
		transaction.setPaymentMode("\'CARD\'");
		transaction.setPaymentAmount(payAmount);
		double convertedAmount = transaction.calculateTotalAmount(order.getAmount(), curr.getCurrencyValue());
		transaction.setConvertedTotalAmount(
				transaction.calculateOrderPayment(convertedAmount, transaction.getPaymentAmount())
				);
		
		int latestId = this.transRepo.save(transaction);
		
		return latestId;
	}
}
