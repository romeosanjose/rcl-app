package com.rcl.core.usecase;

import java.util.HashMap;
import java.util.Map;

import com.rcl.core.entity.Product;
import com.rcl.core.iobound.RCLRequest;
import com.rcl.core.repository.ProductRepositoryInterface;

public class GetProduct {

	private ProductRepositoryInterface productRepository;
	
	public GetProduct(ProductRepositoryInterface productRepository) {
		this.productRepository = productRepository;
	}
	
	public static Map getProductCategory(Product product) {
		HashMap<String, Object> category = new HashMap<String, Object>();
		category.put("id", product.getCategory().getId());
		category.put("name", product.getCategory().getName());
		category.put("description", product.getCategory().getDescription());
		
		return category;
	}
	
	public Map<String, Object> getProduct(RCLRequest request) {
		int id =  Integer.parseInt(request.getData().get("id").toString());
		Product product =  this.productRepository.getProductDetails(id);
		Map productMap = new HashMap<String, Object>();
		productMap.put("id", product.getId());
		productMap.put("name", product.getName());
		productMap.put("description", product.getDescription());
		productMap.put("totalInventory", product.getTotal_inventory());
		productMap.put("availableInventory", product.getAvailable_inventory());
		productMap.put("price", product.getPrice());
		productMap.put("thumbnails", product.getThumbnails());
		productMap.put("category", GetProduct.getProductCategory(product));
		
		return productMap;
	}
}
