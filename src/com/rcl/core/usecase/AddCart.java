package com.rcl.core.usecase;

import java.util.ArrayList;

import com.rcl.core.entity.Cart;
import com.rcl.core.entity.Product;
import com.rcl.core.iobound.RCLRequest;
import com.rcl.core.repository.CartRepositoryInterface;

public class AddCart {

	private CartRepositoryInterface cartRepo;
	
	public AddCart(CartRepositoryInterface cartRepo) {
		this.cartRepo = cartRepo;
	}
	
	public void addProductToCart(RCLRequest request) {
		Product product = new Product();
		product.setId(Integer.parseInt(request.getData().get("id").toString()));
		Cart cart = new Cart();
		cart.setProduct(product);
		cart.setStatus("\'ACTIVE\'");
		int id = this.cartRepo.save(cart);
	}
}
