package com.rcl.core.usecase;

import java.util.ArrayList;
import java.util.Map;

import com.rcl.core.entity.Product;
import com.rcl.core.iobound.RCLRequest;
import com.rcl.core.repository.ProductRepositoryInterface;

public class SearchProduct {
	
	private ProductRepositoryInterface productRepo;
	
	public SearchProduct(ProductRepositoryInterface productRepo) {
		this.productRepo = productRepo;
	}
	
	public ArrayList<Map<String, Object>> searchProduct(RCLRequest req) {
		ArrayList<Product> products = this.productRepo.searchProduct((Map<String, Object>) req.getData().get("search")); 
		ArrayList<Map<String, Object>> productFlat = new ArrayList<Map<String,Object>>();
		for (Product p : products) {
			productFlat.add(p.getFlatEntity());
		}
		return productFlat;
		
	}
}
