package com.rcl.core.usecase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.rcl.core.entity.Product;
import com.rcl.core.entity.ProductCategory;
import com.rcl.core.iobound.RCLRequest;
import com.rcl.core.repository.CategoryRepositoryInterface;
import com.rcl.core.repository.ProductRepositoryInterface;

public class GetProducts {
	
	private ProductRepositoryInterface productRepository;
	
	public GetProducts(
			ProductRepositoryInterface productRepository
			) {
		this.productRepository = productRepository;
	}
	
	public ArrayList<Map> getAllProducts(){
 		ArrayList<Map> productList = new ArrayList<Map>();
		for (Product product : this.productRepository.getProductList()) {
			Map productMap = new HashMap<String, Object>();
			productMap.put("id", product.getId());
			productMap.put("name", product.getName());
			productMap.put("description", product.getDescription());
			productMap.put("totalInventory", product.getTotal_inventory());
			productMap.put("availableInventory", product.getAvailable_inventory());
			productMap.put("price", product.getPrice());
			productMap.put("thumbnails", product.getThumbnails());
			productMap.put("category", GetProduct.getProductCategory(product));
			
			productList.add(productMap);
		}
		
		return productList;
	}
	
	
}
