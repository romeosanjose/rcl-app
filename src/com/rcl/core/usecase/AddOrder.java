package com.rcl.core.usecase;

import java.sql.Array;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.rcl.core.entity.Order;
import com.rcl.core.entity.Product;
import com.rcl.core.entity.User;
import com.rcl.core.iobound.RCLRequest;
import com.rcl.core.repository.OrderRepositoryInterface;
import com.rcl.core.repository.ProductRepositoryInterface;
import com.rcl.core.repository.UserRepositoryInterface;

public class AddOrder {

	private OrderRepositoryInterface orderRepo;
	private ProductRepositoryInterface productRepo;
	private UserRepositoryInterface userRepo;
	
	public AddOrder(OrderRepositoryInterface orderRepo, 
			ProductRepositoryInterface productRepo,
			UserRepositoryInterface userRepo
			) {
		this.orderRepo = orderRepo;
		this.productRepo = productRepo;
		this.userRepo = userRepo;
	}
	
	public void createOrderFromCart(RCLRequest req) {
		Order order = new Order();
		Map<Integer, Integer> productIdItems = (Map<Integer, Integer>) req.getData().get("product_id_items");
		Map<Product, Integer> productObjItems = new HashMap<Product, Integer>();
		Integer productId = null;
		for (Entry<Integer, Integer> entry : productIdItems.entrySet()) {
			productId = entry.getKey();
			Product p = this.productRepo.getProductDetails(productId);
			productObjItems.put(p, entry.getValue());
		}
		int userId = Integer.parseInt(req.getData().get("user_id").toString());
		User user = this.userRepo.getProfile(userId);
		order.createFromCart(productObjItems, user);
		this.orderRepo.createOrder(order);
	}
}
