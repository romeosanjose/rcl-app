package com.rcl.core.usecase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.rcl.core.entity.Order;
import com.rcl.core.entity.Product;
import com.rcl.core.iobound.RCLRequest;
import com.rcl.core.repository.OrderRepositoryInterface;

public class GetOrder {
	
	private OrderRepositoryInterface orderRepo;
	
	public GetOrder(OrderRepositoryInterface orderRepo) {
		this.orderRepo = orderRepo;
	}
	
	public Map<String, Object> getOrder(RCLRequest req) {
		int id = Integer.parseInt(req.getData().get("id").toString());
		Order order = this.orderRepo.getOrder(id);
		Map<String, Object> orderPres = order.getFlatEntity();
		
		ArrayList<Map<Object, Object>> productItemList = new ArrayList<Map<Object,Object>>();
		Map<Object, Object> productItem = new HashMap<Object, Object>();
		Map<String, Object> productDetail = new HashMap<String, Object>();
		Map<String, Integer> itemCount = new HashMap<String, Integer>();
		
		for (Entry<Product, Integer> entryProductItem : order.getProducts().entrySet()) {
			productDetail.put("product", entryProductItem.getKey().getFlatEntity());
			itemCount.put("item_count", entryProductItem.getValue());
			productItem.put(productDetail, itemCount);
			productItemList.add(productItem);
		}
		
		orderPres.put("products_item", productItemList);
		
		return orderPres;
		
	}
}