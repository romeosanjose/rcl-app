package com.rcl.core.iobound;

import java.util.HashMap;

public class RCLRequest {
	
	public HashMap<String, Object> data = new HashMap<String, Object>();

	public HashMap<String, Object> getData() {
		return data;
	}

	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}
	
	
}
