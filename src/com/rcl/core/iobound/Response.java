package com.rcl.core.iobound;

public interface Response {

	public void transform(Object adapter);
}
