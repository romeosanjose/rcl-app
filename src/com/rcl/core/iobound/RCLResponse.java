package com.rcl.core.iobound;

import java.util.ArrayList;

public class RCLResponse {
	
	public ArrayList<Object> data;

	public ArrayList<Object> getData() {
		return data;
	}

	public void setData(ArrayList<Object> data) {
		this.data = data;
	}
	
}
