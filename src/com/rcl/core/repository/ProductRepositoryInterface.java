package com.rcl.core.repository;

import java.util.ArrayList;
import java.util.Map;

import com.rcl.core.entity.Product;

public interface ProductRepositoryInterface extends RepositoryInterface {
	
	public ArrayList<Product> getProductList();
	
	public Product getProductDetails(int id);
	 
	public ArrayList<Product> searchProduct(Map<String, Object> search);
}
