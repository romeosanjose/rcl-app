package com.rcl.core.repository;

import com.rcl.core.entity.Currency;
import com.rcl.core.entity.Order;
import com.rcl.core.entity.Transaction;

public interface TransactionRepositoryInterface extends RepositoryInterface {
	
	public Integer saveTransaction(Transaction transaction);
}
