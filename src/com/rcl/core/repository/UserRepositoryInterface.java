package com.rcl.core.repository;

import com.rcl.core.entity.User;

public interface UserRepositoryInterface extends RepositoryInterface{

	public User getProfile(int id);
}
