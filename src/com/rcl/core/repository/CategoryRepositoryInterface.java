package com.rcl.core.repository;

import java.util.ArrayList;

import com.rcl.core.entity.Product;
import com.rcl.core.entity.ProductCategory;

public interface CategoryRepositoryInterface extends RepositoryInterface {
	
	public ArrayList<ProductCategory> getProductCategoryList();
	
	public Product getProductByCategory(ProductCategory category);

}
