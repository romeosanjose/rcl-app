package com.rcl.core.repository;

import com.rcl.core.entity.Order;
import com.rcl.core.entity.Product;

public interface OrderRepositoryInterface extends RepositoryInterface{
	
	public void createOrder(Order order); 
	
	public Order getOrder(int id);
}
