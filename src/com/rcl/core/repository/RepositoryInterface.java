package com.rcl.core.repository;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;

import com.rcl.core.entity.AbstractEntity;
import com.rcl.core.entity.Product;

public interface RepositoryInterface {
	
	public ResultSet find(int id);
	
	public ResultSet findBy(Map<String, Object> searchMap);
	
	public ResultSet all();
	
	public Integer save(AbstractEntity entity);
	
	public Boolean update(int id, AbstractEntity entity);
}
