package com.rcl.core.repository;

import java.util.ArrayList;
import com.rcl.core.entity.Cart;
import com.rcl.core.entity.Product;

public interface CartRepositoryInterface extends RepositoryInterface {
	
	
	public ArrayList<Product> getProductsFromCart();
}
