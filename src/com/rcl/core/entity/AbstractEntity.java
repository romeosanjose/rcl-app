package com.rcl.core.entity;

import java.util.Map;

public abstract class AbstractEntity {
	
	protected int id;
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public abstract Map<String, Object> getFlatEntity();
}
