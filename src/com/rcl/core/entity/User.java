package com.rcl.core.entity;

import java.util.Map;

import com.rcl.core.iobound.RCLRequest;

public class User extends AbstractEntity{

    private String username;

    private String password;

    private String email;

    private String fullname;

    private boolean card;

    private boolean payPal;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public boolean getCard() {
        return card;
    }

    public void setCard(boolean card) {
        this.card = card;
    }

    public boolean getPayPal() {
        return payPal;
    }

    public void setPayPal(boolean payPal) {
        this.payPal = payPal;
    }

    
   public boolean AuthenticateMember(RCLRequest request)
   {
	   boolean isAuth = false;	   
	   if (request.getData().get("username") == this.getUsername()) {
		   if (request.getData().get("password") == this.getPassword()) {
			   isAuth = true;
		   }
	   }
	 
	   return isAuth;
   }

	@Override
	public Map<String, Object> getFlatEntity() {
		// TODO Auto-generated method stub
		return null;
	}


}
