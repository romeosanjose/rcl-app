package com.rcl.core.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

public class Product extends AbstractEntity{
	private String name;
	private String description;
	private int total_inventory;
	private int available_inventory;
	private double price;
	private String thumbnails;
	private ProductCategory category;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getTotal_inventory() {
		return total_inventory;
	}
	public void setTotal_inventory(int total_inventory) {
		this.total_inventory = total_inventory;
	}
	public int getAvailable_inventory() {
		return available_inventory;
	}
	public void setAvailable_inventory(int available_inventory) {
		this.available_inventory = available_inventory;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getThumbnails() {
		return thumbnails;
	}
	public void setThumbnails(String thumbnails) {
		this.thumbnails = thumbnails;
	}	
	
	public ProductCategory getCategory() {
		return category;
	}
	public void setCategory(ProductCategory category) {
		this.category = category;
	}
	
	public ArrayList<String> arrayedThumbNails(String thumbNails){
		ArrayList<String> arrThumb = new ArrayList<String>();
		JSONObject json = new JSONObject(thumbNails);
		JSONArray jsonArr = json.getJSONArray("thumbnails");
		for (int i = 0; i < jsonArr.length(); i++) {
			arrThumb.add((String) jsonArr.get(i));
		}
		
		return arrThumb;
	}
	@Override
	public Map<String, Object> getFlatEntity() {
		Map<String, Object> flat = new HashMap<String, Object>();
		flat.put("id", this.getId());
		flat.put("name", this.getName());
		flat.put("description", this.getDescription());
		flat.put("total_inventory", this.getTotal_inventory());
		flat.put("available_inventory", this.getAvailable_inventory());
		flat.put("price", this.getPrice());
		flat.put("thumbnails", this.getThumbnails());
		flat.put("category_id", this.getCategory().getId());
		
		return flat;
	}
	
	
}
