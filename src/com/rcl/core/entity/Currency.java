package com.rcl.core.entity;

import java.util.HashMap;
import java.util.Map;

public class Currency extends AbstractEntity{
	
	private String currency;
	
	private double currencyValue;
	
	public static String DEFAULT_CURRENCY = "PHP";

	public double getCurrencyValue() {
		return currencyValue;
	}

	public void setCurrencyValue(double currencyValue) {
		this.currencyValue = currencyValue;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public Map<String, Object> getFlatEntity() {
		Map<String, Object> flat = new HashMap<String, Object>();
		flat.put("id", this.getId());
		flat.put("currency", this.getCurrency());
		flat.put("value", this.getCurrencyValue());
		
		return flat;
		
	}
	
	
	
}
