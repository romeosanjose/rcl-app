package com.rcl.core.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Order extends AbstractEntity {
	
	private double amount;

	private Map<Product, Integer> products = new HashMap<Product, Integer>();
	
	private User user;
	
	private String status;
	
	private Map<String, Object> flatEntity;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Map<Product, Integer> getProducts() {
		return products;
	}

	public void setProducts(Map<Product,Integer> products) {
		this.products = products;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public void createFromCart(Map<Product, Integer> selectedProducts, User user) {
		double totalAmount = 0;
		for (Entry<Product, Integer> entry : selectedProducts.entrySet()) {
			Product product = entry.getKey();
			Integer itemCount = entry.getValue();
			totalAmount = totalAmount + (product.getPrice() * itemCount);
			this.products.put(product, itemCount);
		}
		this.setAmount(totalAmount);
		this.setUser(user);
		this.setStatus("\'ACTIVE\'");
	}

	@Override
	public Map<String, Object> getFlatEntity() {
		this.flatEntity =  new HashMap<String, Object>();
		flatEntity.put("id", this.getId());
		flatEntity.put("amount", this.getAmount());
		flatEntity.put("status", this.getStatus());
		flatEntity.put("user_id", this.getUser().getId());
		
		return flatEntity;
		
	}
	
	public void setFlatEntity(Map<String, Object> flatEntity) {
		this.flatEntity = flatEntity;
	}
}
