package com.rcl.core.entity;

import java.util.Map;

public class ProductCategory extends AbstractEntity{
	private String name;
	private String description;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public Map<String, Object> getFlatEntity() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
