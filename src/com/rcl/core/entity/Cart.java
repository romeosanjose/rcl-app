package com.rcl.core.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Cart extends AbstractEntity {
	
	private Product product;
	
	private String status;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public Map<String, Object> getFlatEntity() {
		// TODO Auto-generated method stub
		Map<String, Object> fields = new HashMap<String, Object>();
		fields.put("id", this.getId());
		fields.put("product_id", this.getProduct().getId());
		fields.put("status", this.getStatus());
		return fields;
	}
}
