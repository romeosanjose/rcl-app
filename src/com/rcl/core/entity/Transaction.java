package com.rcl.core.entity;

import java.util.HashMap;
import java.util.Map;

public class Transaction extends AbstractEntity{

	private Order order;
	
	private Currency orderCurrency;
	
	private double convertedTotalAmount;
	
	private String paymentMode;
	
	private double paymentAmount;
	
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Currency getOrderCurrency() {
		return orderCurrency;
	}

	public void setOrderCurrency(Currency orderCurrency) {
		this.orderCurrency = orderCurrency;
	}

	public double getConvertedTotalAmount() {
		return convertedTotalAmount;
	}

	public void setConvertedTotalAmount(double convertedTotalAmount) {
		this.convertedTotalAmount = convertedTotalAmount;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public double calculateTotalAmount(double totalAmount, double currencyValue ) {
		return totalAmount * currencyValue;
		
	}
	
	public double calculateOrderPayment(double convertedAmount, double paymentAmount) throws Exception {
		if (paymentAmount < convertedAmount) {
			throw new Exception("Insufficient Payment Amount");
		}
		
		return paymentAmount - convertedAmount;
	}

	@Override
	public Map<String, Object> getFlatEntity() {
		Map<String, Object> flatEntity = new HashMap<String, Object>();
		flatEntity.put("id", this.getId());
		flatEntity.put("order_id", this.getOrder().getId());
		flatEntity.put("currency_id", this.getOrderCurrency().getId());
		flatEntity.put("converted_total_amount", this.getConvertedTotalAmount());
		flatEntity.put("payment_mode", this.getPaymentMode());
		flatEntity.put("payment_amount", this.getPaymentAmount());
		
		return flatEntity;
	}
}
