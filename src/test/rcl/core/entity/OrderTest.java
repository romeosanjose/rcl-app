package test.rcl.core.entity;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;


import org.junit.jupiter.api.Test;

import com.rcl.core.entity.Order;
import com.rcl.core.entity.Product;
import com.rcl.core.entity.User;

class OrderTest {

	@Test
	void createOrderFromCartSelectedProducts() {
		User user = new User();
		user.setId(1);
		ArrayList<Product> products = new ArrayList<>();
		Product p1 = new Product();
		Product p2 = new Product();
		Product p3 = new Product();
		p1.setPrice(10.00);
		p2.setPrice(20.50);
		p3.setPrice(125.99);
		products.add(p1);
		products.add(p2);
		products.add(p3);
		Order order = new Order();
		HashMap<Product, Integer> cart = new HashMap<>();
		cart.put(p1, 2);
		cart.put(p2, 3);
		cart.put(p3, 1);
		order.createFromCart(cart, user);
		assertEquals(207.49, order.getAmount());
	}
}
