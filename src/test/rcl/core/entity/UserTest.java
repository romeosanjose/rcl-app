package test.rcl.core.entity;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;

import org.junit.jupiter.api.Test;

import com.rcl.core.iobound.RCLRequest;
import com.rcl.core.entity.User;

class UserTest {

	@Test
	void testWrongUsername() {
		User user = new User();
		user.setUsername("superman");
		user.setPassword("password123");
		RCLRequestMock request = new RCLRequestMock();
		HashMap<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("username", "batman");
		userMap.put("password", "password123");
		request.setData(userMap);
		assertFalse(user.AuthenticateMember(request));
	}
	
	@Test
	void testSuccessfulLogin() {
		User user = new User();
		user.setUsername("superman");
		user.setPassword("password123");
		RCLRequestMock request = new RCLRequestMock();
		HashMap<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("username", "superman");
		userMap.put("password", "password123");
		request.setData(userMap);
		assertTrue(user.AuthenticateMember(request));
	}
}


