package test.rcl.core.entity;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.rcl.core.entity.Product;

class ProductTest {

	@Test
	void convertThumbNailsToArrayList() {
		String thumbNail = "{thumbnails:[item1.png, item2.png, item3.png]}";
		Product product = new Product();
		assertEquals(3, product.arrayedThumbNails(thumbNail).size());
	}

}
